# SONARWORKS DOCUMENTS TEST GUIDANCE

## Test project short introduction
*Additionaly to usual requirements i have added additional logic to introduce a wide technology stack use
1. Added custom artisan command which parse all documents and saves it into database (path App\Console\Commands\FetchPages )

2. To get additional documents from external resource simply add required document name endpoint to pages config file array https://prnt.sc/13b7gdj

3. Additionaly added Vuex and VueRouter to introduce this technology usage in front end part

4. Docker introduction part :

	* 4.1 Database and application entities are located in separate containers (Database layer is added to introduce the work between webserver and db containers and also to introduce laravel and mysql work)
	
	* 4.2 To see which port belongs to webserver container and which port to db container, should open docker-compose.yml file https://prnt.sc/13b7vyd
	
	* 4.3 All webserver's and php configurations are located in devops folder https://prnt.sc/13b7z5t
	
	* 4.4 All required connection configurations with database inside docker container are located in .env.example file, which should be renamed on .env file
	
5. If there will be some additional questions, do not hesitate to message me via email :)
 
## Commands to execute
```sh
docker-compose up --build
```
> Install php packages inside app container
```sh
docker-compose run app composer install
```
> Generate project encryption key inside app container (this command should be executed after composer install)
```sh
docker-compose run app php artisan key:generate
```
> Make migrations inside app container (this command should be executed after composer install)
```sh
docker-compose run app php artisan migrate
```

> Parse documents from sonarworks external resource and save them into DB 
> (this command should be executed after composer install and migration command (php artisan:migrate))
```sh
docker-compose run app php artisan pages:import
```

> Install node modules (front part)
```sh
docker-compose run app php npm install
```

> Run front part with with production flag (this command should be executed after npm install or npm i)
```sh
docker-compose run app php npm run prod
```
